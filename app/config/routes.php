<?php

use System\Core\URL;
use System\Core\Route;
use System\Core\Router;
use System\Core\HTTPTypesEnum;



$mainPageURL = new URL('^/blog/post(/[0-9]*)$', 'blog', 'post');
$mainPageRoute = new Route( $mainPageURL, new HTTPTypesEnum( HTTPTypesEnum::GET) );
Router::addRoutes( [$mainPageRoute] );