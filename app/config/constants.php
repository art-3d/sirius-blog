<?php
/**
 * LOG LEVEL
 */
define('LOG_LEVEL', \System\Core\Log::LOG_BOTH);
/**
 * ACTIVE DATABASE CONNECTION
 */
define('ACTIVE_CONNECTION', 'dev');

/**
 * Enable annotations
 */
define('ENABLE_ANNOTATIONS', true);
/**
 * use  OUTPUT BUFFER ?
 */
define('OUTPUT_BUFFER', true);
/**
 * USE CACHE ?
 */
define('USE_CACHE', true);
/**
 * DEBUG MODE
 */
define('DEBUG_MODE', false);
/**
 * ROUTER CONSTANTS
 */
define('CONTROLLER_SEGMENT', 1);
define('ACTION_SEGMENT', 2);
define('PARAMS_SEGMENT', 3);
define('DEFAULT_CONTROLLER', 'blog');
define('DEFAULT_ACTION', 'index');
/**
 * CACHE PATH
 */
define('CACHE_PATH', BASE_PATH . 'app/cache/');
/**
 * File modes
 */
define('FOPEN_WRITE_CREATE', 'ab');
define('FILE_WRITE_MODE', 0666);
/**
 * SESSION SETTINGS
 * TYPE:
 * 1 - COOKIE
 * 2 - PHP SESSION
 * 3 - BOTH
 */

define('SESSION_TYPE', 2);
//The encryption key. It must be of length 16, 24, or 32 in order
define('SESSION_KEY', '8NpFQCGQPawhiNg2');
define('COOKIE_EXPIRE', 3600);
define('COOKIE_PATH', '/');
define('COOKIE_DOMAIN', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
define('COOKIE_SECURE', true);
define('COOKIE_HTTP_ONLY', false);
/**
 * CUSTOM ROUTES
 */
define('USE_CUSTOM_ROUTES', false);
/**
 * VALIDATION ERRORS LANGUAGES KEY
 */
define('VALIDATION_ERROR_LANG_KEY', 'en');