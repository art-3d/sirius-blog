<?php

return [
    'dev' => [
        'host' => 'localhost',
        'user' => 'root',
        'password' => 'root',
        'database' => 'sirius',
        'charset'  => 'utf8',
        'driver'   => DATASOURCE_MYSQL
    ]
];