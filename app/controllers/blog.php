<?php


namespace Controllers;

use Models\Post;
use \System\Core\BaseController;
use System\Core\Log;
use System\DataBase\MysqlQueryBuilder;
use System\Email\MailConfiguration;
use System\Email\SendMail;
use System\Email\SMTPMail;
use System\Email\StandartMail;

use System\Core\Request;
use System\Validation\Validator;
use System\Validation\Rule;


/**
 * Class Blog
 * @package Controllers
 *
 */
class Blog extends BaseController
{

    public function index()
    {
       $model = $this->getLoader()->model('post');
       $data = $model->getPosts();

       ob_start();
       $this->getLoader()->view('index_page', [
          'posts' => $data,
          'header' => 'Posts:',
           ]);
       $content = ob_get_clean();

       $this->getLoader()->view('layout', [
          'content' => $content,
          'title' => 'Blog'
        ]);
    }

    public function add()
    {
      $request = new Request();
      if ( $request->isPost() ){

        $titleRule = new Rule('title', 'Title');
        $titleRule->isRequired()->minLength(5)->maxLength(100);
        $textRule = new Rule('text', 'Text');
        $textRule->isRequired()->minLength(50);

        $validator = new Validator(Request::METHOD_POST, MysqlQueryBuilder::getInstance());
        $validator->addRules( [$titleRule, $textRule] );

        if ( !$validator->run() ) {

          print_r( $validator->getErrors() );
          throw new \Exception('<h2>Invalid data</h2>');
          return;
        }

        $model = $this->getLoader()->model('post');

        $title = $request->post('title');
        $text = $request->post('text');

        $model->addPost($title, $text);

        header('Location: /');
      }

      ob_start();
      $this->getLoader()->view('add_page');
      $content = ob_get_clean();

      $this->getLoader()->view('layout', ['content' => $content, 'title' => 'Add post']);
    }

    public function post()
    {
       $request = new Request();
       $id = (int)$request->segment();

       if ( $id > 0) {

       $model = $this->getLoader()->model('post');
       $post = $model->getPost($id);
       $model->addView($id);

        ob_start();
        $this->getLoader()->view( 'post_page', ['post' => array_shift($post) ] );
        $content = ob_get_clean();

        $this->getLoader()->view('layout', ['content' => $content, 'title' => $post->title]);
      } else {
        $this->index();
      }

    }

    public function top()
    {
      $max = 3;

       $model = $this->getLoader()->model('post');
       $data = $model->getTopPosts($max);

       ob_start();
       $this->getLoader()->view('index_page', [
          'posts' => $data,
          'header' => "Top $max posts:",
           ]);
       $content = ob_get_clean();

       $this->getLoader()->view('layout', [
          'content' => $content,
          'title' => 'Blog'
        ]);
    }

}