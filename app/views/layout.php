<!DOCTYPE html>
<html>
<head>
  <title><?php echo $title ?></title>
  <link rel="stylesheet" href="/css/style.css" />
</head>
<body>

  <div class="nav">
    <ul>
      <li><a href="/">Home</a></li>
      <li><a href="/blog/top">Top</a></li>
      <li><a href="/blog/add">Add</a></li>
      <li class="right"><a href="/blog/about">About</a></li>
    </ul>
  </div>

  <div class="wrapper">
    <?php echo $content ?>
  </div>

  <!-- javascript -->
  <script src="/js/script.js"></script>

</body>
</html>