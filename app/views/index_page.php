  <h2><?php echo $header ?></h2>
<?php foreach($posts as $post): ?>
  <article>
    <h3>
      <a href="/blog/post/<?php echo $post->id ?>"><?php echo $post->title; ?></a>
    </h3>
    <div class="text">
    <?php echo substr($post->text, 0, 175).'...'; ?>
    </div>
    <p class="created"><?php echo date('l jS \of F Y H:i:s', $post->created) ?></p>
  </article>
<?php endforeach; ?>
