<?php

namespace Models;

use System\Core\BaseModel;
use System\DataBase\STMTReturn;
use System\Interfaces\IQueryBuilder;

/**
 * Class Post
 * @package Models
 * @property IQueryBuilder $_db - database query builder
 */
class Post extends BaseModel
{
  public function getPosts()
  {
        /**
         * @var STMTReturn $data
         */
        $data = $this->_db->query('SELECT * FROM post', []);

        if (!$data->numRows()){
            throw new \Exception("No posts\n");
        }

        return $data->resultObject();
  }

  public function getPost($id)
  {
        $data = $this->_db->query("SELECT * FROM post WHERE id='$id'", []);

        if ( !$data->numRows() ) {
            throw new \Exception("No posts\n");
        }

        return $data->resultObject();
  }

  public function getTopPosts($limit)
  {
    $data = $this->_db->query("SELECT * FROM post ORDER BY views DESC LIMIT $limit", []);

    if (!$data->numRows()){
        throw new \Exception("No posts\n");
    }

    return $data->resultObject();
  }

  public function addView($id)
  {
    $this->_db->query("UPDATE post SET views=views+1 WHERE id=$id");
  }

  public function addPost($title, $text)
  {
      $created = time();
      $text =  '<p>'. str_replace("\n", '</p><p>', $text). '</p>' ;
      $this->_db->query("INSERT INTO post (title, `text`, created) VALUES('$title', '$text', $created)" );
  }

}